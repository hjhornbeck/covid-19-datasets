# COVID-19 Datasets (2023.10 - ...)

A collection of COVID-19-related datasets, drawn from public sources. The goal of this 
 project is to archive as much of the raw statistics as possible in a central
 location, to make it easier for future researchers to study. Additionally, we
 hope to make this collection self-sustaining, so that it automatically adds
 new datasets and maintains old ones. We also hope to keep as much temporal
 information as possible.

Using a source control system to collect these datasets has a number of 
 advantages. Many software projects have some level of automation tied to
 source control, which can automatically compile the code or create commits to
 a code repository. We can leverage that automation to automatically download
 and process source data, updating the repository as necessary. This allows it
 to automatically update itself, only requiring human intervention when
 an automated script breaks.

Source control systems also keep track of changes over time. This can be useful
 for datasets that do not contain temporal information, for instance a summary
 table of the current COVID-19 cases in a specific region. Researchers who are
 interested in historic data can use the repository history to retrieve older
 data, while the uninterested can simply use the latest snapshot. COVID-19
 datasets often combine presumed cases with confirmed ones, which creates 
 problems when some of those presumed cases are disconfirmed as COVID. The usual
 solution is to silently update the latest data, leading to situations where the
 number of cases can drop. These have not received much study, so capturing these
 situations via the repository's history could prove valuable.

Finally, git repositories in particular have a feature called "[submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)",
 which allows one repository to incorporate data from another. This permits
 datasets to link to one another, reducing duplication.

GitLab places limitations on the amount of space a repository takes up, which
 does not combine well with automatic background updates. The solution is to
 archive old repositories as the come up against this limit and create new ones.
 For ease of access, the latest repository will always be at [this url](https://gitlab.com/hjhornbeck/covid-19-datasets).
 Archived repositories can be found at:

* [2020 to October 23rd, 2023](https://gitlab.com/hjhornbeck/covid-19-datasets-2020-2023.10)

## Datasets present

The following datasets are currently being archived:

* [Alberta Health's Respiratory Virus Dashboard](https://www.alberta.ca/stats/dashboard/respiratory-virus-dashboard.htm), which also includes data on influenza, rhinovirus, respiratory syncytial virus (RSV), adenovirus, and human metapneumovirus.
* [CDC wastewater data](https://covid.cdc.gov/covid-data-tracker/#wastewater-surveillance)
* [Canadian Health Infobase data](https://health-infobase.canada.ca/covid-19/)
* [Canada Respiratory Virus Surveillance report](https://health-infobase.canada.ca/respiratory-virus-surveillance/)

## Structure of the Archive

As implied by their names, the [data](data/) directory contains the data being
 captured by the archive. Further details of the format are contained via a
 README file in that directory.

The [scripts](scripts/) directory contains the script files used by the 
 repository's automation. The details of what is executed is contained 
 within [gitlab-ci.yml](.gitlab-ci.yml), which contains instructions
 for GitLab on what to run and in what order; details of the format are [available here](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html).

The license for the source code is available [here](LICENSE).
