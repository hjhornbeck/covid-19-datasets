# Raw data for Canada

While health care is a provincial responsibility, the federal government of Canada also maintains [a health department](https://www.canada.ca/en/public-health.html) focused on prevention and monitoring emerging health threats. Consequently, from the beginning of the COVID-19 pandemic they have been aggregating relevant provincial statistics and [publishing them to the Public Health Infobase](https://open.canada.ca/data/en/dataset/261c32ab-4cfd-4f81-9dea-7b64065690dc). The resulting product, [covid19-download.csv](covid19-download.csv), focuses primarily on case counts and fatalities, both in terms of raw numbers and sliding-window averages. See [the data dictionary](covid19-casesdeaths-data-dictionary.csv) for further information.

# Raw data for Alberta

The Alberta government publishes [an excellent dashboard](https://www.alberta.ca/stats/covid-19-alberta-statistics.htm), 
 full of interactive charts. The data for those charts is contained in a 
 series of JSON files containing all the information [Plotly](https://plotly.com/chart-studio-help/json-chart-schema/) 
 needs to display the chart.
 
This archive records those files in a directory reflecting when that data
 was captured; the directory names are in year-month-day, hour-minute-second 
 format. Within each directory is a "map.tsv" file which records the order
 those JSON files were encountered on the dashboard, and at minimum three 
 other columns of data:

* **file**: The filename containing that chart's data, minus the '.json'.
* **figure**: The title associated with that chart.
* **geographic**: Either "True" if that JSON is geographic data, usually 
 presented via [Leaflet](https://leafletjs.com/index.html), or any other
 value if it is a Plotly chart.

Additional columns correspond to annotations placed on the associated axes 
 or legend of the chart. These may be blank if that annotation wasn't present. 

As Alberta has changed the format of their charts many times during the 
 pandemic, this "map.tsv" file is very useful for tracking those changes.
