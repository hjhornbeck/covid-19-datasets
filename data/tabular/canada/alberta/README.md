# Tabular data for Alberta

Alberta's [COVID-19 dashboard](https://www.alberta.ca/stats/covid-19-alberta-statistics.htm)
 had a "Data Export" tab where you could download tabular data. This data
 used to be embedded in the webpage itself, but starting in April 2021 it
 was separated into a separate download. Four series were represented until late
 2022, when the anonymized case data was delisted from the site; the underlying
 dataset was still available at the original URL, however, and was still updated
 until August 16th, 2023. As of October 2nd, 2023, the remaining three series are no
 longer being updated.

## covid-19-alberta-statistics-data.csv

Partly-anonymized data on specific cases, containing information on:

* The date reported to Alberta Health,
* The [Alberta Health Zone](https://open.alberta.ca/dataset/a14b50c9-94b2-4024-8ee5-c13fb70abb4a/resource/70fd0f2c-5a7c-45a3-bdaa-e1b4f4c5d9a4/download/official-standard-geographic-area-document.pdf)
 it was reported to,
* The gender of the patient,
* The age group that patient belongs to,
* Whether they died of COVID,
* And whether it was a confirmed or probable case of COVID.

## covid-19-alberta-statistics-map-data.csv

Summary statistics on COVID cases for a variety of geospatial divisions 
 within Alberta. The data includes:

* The date in question;
* The type of region, which could be a [Local Geographic Area](https://open.canada.ca/data/en/dataset/01e60fcd-cfda-4ba3-b08f-5abf5e582442)
 or a [municipality](https://www.alberta.ca/municipal-locations-and-codes.aspx);
* The name of the region itself. There are 133 Local Geographic Areas, 
 and 110 municipalities;
* The cumulative number of cases;
* The estimated population of that LGA/municipality.

## covid-19-alberta-statistics-summary-data.csv

Summary statistics related to COVID for the entire province. This includes:

* That date in question;
* The number of lab tests performed that day;
* The cumulative number of lab tests done to that date;
* The number of cases that day;
* The cumulative number of cases as of that date;
* The number of COVID patients currently hospitalized that day;
* The number of COVID patients currently in the ICU that day;
* The number of deaths due to COVID that day;
* The cumulative number of deaths due to COVID;
* The number of variants of concern detected that day;
* The percent of COVID-19 tests administered that came back positive.

## lga-coverage.csv

Summary statistics for vaccinations administered, organized by Local 
 Geographic Area. This includes:

* The code used to represent that LGA;
* The name of the LGA itself;
* Which of the five health zones it is located in;
* The age group this vaccination information applies to. This could be a
 bounded or unbounded range;
* The estimated population for that age group within that LGA;
* The number of people with one vaccine dose;
* The number with two vaccine doses;
* The number with three vaccine doses;
* The percentages of the age group and LGA population for the above numbers;
* The percentages of the age group for the above numbers, across all of Alberta.

