# external

[This directory](external/) is for external git repositories of COVID-19 data.
 These may be referenced by this repository's scripts.

# raw

[This directory](raw/) contains any raw data files necessary for any generated
 tables, and also serves as a place for historic data.

# generated

[This directory](generated/) contains the aforementioned automatically-generated
 tables based on the contents of [external](external/) and [raw](raw/).

# tabular

[This directory](tabular/) is reserved for tabular datasets drawn from external
 sources that are not git repositories. Unlike the files in [generated](generated/),
 these files are drawn from their sources as-is and have not been processed
 by this repository.
