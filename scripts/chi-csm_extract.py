#!/usr/bin/env python3

import gzip
import json
import pandas as pd
from urllib.request import urlopen, Request

headers = { "User-Agent": 'gitlab.com/hjhornbeck/covid-19-datasets' }


def url_to_file( target, headers ):
    """Use urllib to transform a URL into a filehandle object."""
    incoming = urlopen( Request( target, headers=headers ) )
    if incoming.headers['Content-Encoding'] == 'gzip':
        incoming = gzip.open( incoming, mode='rt' )
    return incoming

# a one-liner to handle the main wastewater dataset
pd.read_json('https://covid-tracker-json.s3.us-west-2.amazonaws.com/wasteWaterAbData.json',orient='split').set_index(['location','date']).to_csv('generated/canada/alberta/wasteWaterAbData.csv')

# we need more in order to extract the variants of concern
dataset = pd.read_json('https://covid-tracker-json.s3.us-west-2.amazonaws.com/vocData.json',orient='split')
slurp = {'BA.1':[], 'BA.2':[], 'BA.4&BA.5':[]}
for idx,row in dataset.iterrows():
    assert len(row['variants']) == len(slurp)
    for key in slurp.keys():
        assert key in row['variants']

    slurp['BA.1'].append( row['variants']['BA.1'] )
    slurp['BA.2'].append( row['variants']['BA.2'] )
    slurp['BA.4&BA.5'].append( row['variants']['BA.4&BA.5'] )

for key in slurp.keys():
    dataset[key] = slurp[key]

dataset.drop(['variants'],axis=1).set_index(['location','date']).to_csv('generated/canada/alberta/wasteWaterAbVoc.csv')


# the newer datasets:
dataset = json.load( url_to_file('https://chi-covid-data.pages.dev/aplWasteWaterAbData.json',headers) )

storage = { 'date':[], 'location':[], 'n1':[], 'n2':[], 'avg':[] }
for location in dataset['data'].keys():
    for row in dataset['data'][location]:
        if 'date' not in row:
            continue
        n1   = row['n1']  if 'n1' in row else float('NaN')
        n2   = row['n2']  if 'n2' in row else float('NaN')
        avg  = row['avg'] if 'avg' in row else float('NaN')

        storage['date'].append( row['date'] )
        storage['location'].append( location )
        storage['n1'].append( n1 )
        storage['n2'].append( n2 )
        storage['avg'].append( avg )

pd.DataFrame( storage ).to_csv( 'generated/canada/alberta/aplWasteWaterAbData.csv', index=False )


# handle RSV and influenza data as well
dataset = pd.read_json(url_to_file('https://chi-covid-data.pages.dev/rsvFlu.json',headers), orient='split')

storage = { 'date':[], 'location':[], 'target':[], 'v':[], 'sd':[] }
for target,row in dataset.iterrows():
    for location in row.keys():
        for date in row[location]:
            v = row[location][date]['v'] if 'v' in row[location][date] else float('NaN')
            sd = row[location][date]['sd'] if 'sd' in row[location][date] else float('NaN')

            storage['date'].append( date )
            storage['location'].append( location )
            storage['target'].append( target )
            storage['v'].append( v )
            storage['sd'].append( sd )

pd.DataFrame( storage ).to_csv( 'generated/canada/alberta/rsvFlu.csv', index=False )
