#!/usr/bin/python3

##### IMPORTS

import json
from os import listdir
import pandas as pd
from pathlib import Path
import re
import sys


##### GLOBALS

season = re.compile( r'(\d\d\d\d)-\d\d\d\d season' )


##### FUNCTIONS
def extract_scatter( file, column='', index='date' ):
    """Extract Plotly scatterplot data from a JSON file. Returns a pandas
        dataframe."""

    # open the matching JSON
    with open( file, 'rt' ) as f:
        data = json.load( f )
    
    # check if there's any drop-downs associated with this
    if ('layout' in data['x']) and ('updatemenus' in data['x']['layout']) and \
        (len(data['x']['layout']['updatemenus']) > 0) and \
        ('buttons' in data['x']['layout']['updatemenus'][0]):

            # if yep, collect them all
            categories = dict()
            for row in data['x']['layout']['updatemenus'][0]['buttons']:
                if ('args' in row) and ('label' in row) and (row['args'][0] == 'visible'):
                    categories[row['label']] = row['args'][1]

    else:
        categories = {}

    # extract the tabular data
    output = None
    for idx in range( len(data['x']['data']) ):

        if 'x' not in data['x']['data'][idx]:
            continue            # no data in the series

        if 'name' in data['x']['data'][idx]:
            colname = data['x']['data'][idx]['name']
        else:
            colname = column

        # prepend with the category, if applicable
        if len(categories) > 0:
            for prepend,mask in categories.items():
                if mask[idx]:
                    colname = prepend + ' - ' + colname
                    break

        temp = pd.DataFrame( data=zip(data['x']['data'][idx]['x'],data['x']['data'][idx]['y']), \
            columns=[index,colname] )
        temp.set_index( index, inplace=True )
        mask = ~temp.index.duplicated(keep='first')   # weed out duplicates (credit: https://stackoverflow.com/questions/13035764/remove-pandas-rows-with-duplicate-indices/34297689#34297689)

        if idx == 0:
            output = temp[mask]
        else:
            output = pd.concat( [output,temp[mask]], axis=1, sort=True )
            output.index.name = index

    return output


##### MAIN

# consolidate the first argument's directories into a sorted list
years = sorted( [d for d in listdir(sys.argv[1])] )
current_year = f'{sys.argv[1]}/{years[-1]}'
dirs = sorted( [d for d in listdir(current_year)] )
target = f'{current_year}/{dirs[-1]}'

# open the map file
mapping = pd.read_csv( f'{target}/map.tsv', sep='\t' )

# for each row, try to find a matching figure
for idx,row in mapping.iterrows():

    # ignore geographic data, that's handled elsewhere
    if row['geographic'] == True:
        continue

    # check if the file exists
    if not Path(f"{target}/{row['file']}.json").exists():
        print(f'WARNING: The mapping file "{target}/map.tsv" referenced a file that does not exist,' + \
            f' "{target}/{row["file"]}.json". It likely was deleted bacause it was identical to another one,' + \
            " but if this error becomes common it could be a sign something has gone wrong.")
        continue

    # ensure row['figure'] is a string
    if type( row['figure'] ) is not str:
        continue

        
    if row['figure'] and row['figure'][:59] == 'Figure 1: COVID-19 cases in Alberta by day and case status.':

        output = extract_scatter( f"{target}/{row['file']}.json" ) 

        # write to a CSV (note the distinctive 'covid19')
        output.to_csv( f'{sys.argv[2]}/covid19_cases_by_date.csv' )

    elif row['figure'] and row['figure'][:49] == 'Figure 2: COVID-19 cases in Alberta by age group.':

        output = extract_scatter( f"{target}/{row['file']}.json" )

        # write to a CSV
        if 'per 100,000' in row['yaxis']:
            output.to_csv( f'{sys.argv[2]}/covid19_cases_by_date_by_age__per_100000.csv' )
        elif '(n)' in row['yaxis']:
            output.to_csv( f'{sys.argv[2]}/covid19_cases_by_date_by_age__n.csv' )

    elif row['figure'] and row['figure'][:44] == "Figure 3: Number of outbreaks by week opened":

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_outbreaks_by_week.csv' )

    elif row['figure'] and row['figure'][:32] == "Number of COVID-19 vaccine doses":

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/vaccine_doses_by_date.csv' )

    elif row['figure'] and row['figure'][:55] == "Cumulative percent of individuals who received one dose":

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/vaccine_coverage_by_date.csv' )

    elif row['figure'] and row['figure'][:54] == "Vaccine coverage by day, dose and age group in Alberta":

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/vaccine_coverage_by_date_by_age.csv' )

    elif row['figure'] and row['figure'][:47] == "Number of adverse events following immunization":

        output = extract_scatter( f"{target}/{row['file']}.json", column='event type', index='count' )
        output.to_csv( f'{sys.argv[2]}/adverse_events.csv' )

    elif row['figure'] and row['figure'][:56] == 'Figure 4: Number of COVID-19 cases currently in hospital':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_current_primary_hospitalizations_by_date.csv' )

    elif (row['figure'] and row['figure'][:57] == 'Figure 4: Number of current COVID-19 patients in hospital') \
     or (row['figure'] and row['figure'][:54] == 'Figure 5: Number of current COVID-19 cases in hospital'):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_current_hospitalizations_by_date.csv' )

    elif row['figure'] and row['figure'][:58] == 'Figure 6: Rate of new hospitalizations related to COVID-19':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_primary_hospitalizations_per_million_by_date.csv' )

    elif (row['figure'] and row['figure'][:39] == 'Figure 5: Rate of new hospitalizations ') \
     or (row['figure'] and row['figure'][:58] == 'Figure 7: Rate of new hospitalizations related to COVID-19'):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_hospitalizations_per_million_by_date.csv' )

    elif (row['figure'] and row['figure'][:43] == 'Figure 6: Daily COVID-19 attributed deaths.') \
     or (row['figure'] and row['figure'][:43] == 'Figure 8: Daily COVID-19 attributed deaths.'):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_deaths_by_date.csv' )

    elif row['figure'] and row['figure'][:49] == 'Figure 7: Intensive Care Unit (ICU) bed capacity.':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/icu_bed_capacity_by_date.csv' )

    elif row['figure'] and row['figure'][:43] == 'Figure 8: Total ICU bed capacity over time.':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/icu_beds_by_date.csv' )

    elif row['figure'] and row['figure'][:31] == 'Figure 9: Non-ICU bed capacity.':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/hospital_bed_capacity_by_date.csv' )

    elif (row['figure'] and row['figure'][:92] == 'Figure 10: Cumulative COVID-19 cases in Alberta by zone and date reported to Alberta Health.') \
     or (row['figure'] and row['figure'][:91] == 'Figure 9: Cumulative COVID-19 cases in Alberta by zone and date reported to Alberta Health.'):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_cases_by_date_by_zone.csv' )

    elif (row['figure'] and row['figure'][:58] == 'Figure 11: Tests performed for COVID-19 in Alberta by day.') \
     or (row['figure'] and row['figure'][:58] == 'Figure 10: Tests performed for COVID-19 in Alberta by day.'):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_tests_by_date.csv' )

    elif (row['figure'] and row['figure'][:77] == 'Figure 12: Cumulative and daily test positivity rate for COVID-19 in Alberta.') \
     or (row['figure'] and row['figure'][:77] == 'Figure 11: Cumulative and daily test positivity rate for COVID-19 in Alberta.'):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_test_positivity_by_date.csv' )

    elif row['figure'] and row['figure'][:59] == 'Figure 12: Positivity rate for COVID-19 in Alberta by zone.':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_test_positivity_by_zone.csv' )

    elif (row['figure'] and row['figure'][:63] == 'Figure 14: Variant of concern COVID-19 cases in Alberta by day.') \
     or (row['figure'] and row['figure'][:63] == 'Figure 13: Variant of concern COVID-19 cases in Alberta by day.'):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_variants_by_date.csv' )

    elif row['figure'] and row['figure'][:47] == 'Figure 14: Average quantity of SARS CoV-2 virus':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.to_csv( f'{sys.argv[2]}/covid19_wastewater_quantity.csv' )

    elif row['figure'].startswith( 'Laboratory-confirmed influenza by age group, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_labconf_by_age_group__per_100000{years}.csv' )

    elif row['figure'].startswith( 'Laboratory-confirmed COVID-19 by age group, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid_labconf_by_age_group__per_100000{years}.csv' )

    elif row['figure'].startswith( 'Laboratory confirmed seasonal influenza cases in Alberta by zone, 2019' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_labconf_by_zone.csv' )

    elif row['figure'].startswith( 'Laboratory confirmed seasonal influenza cases in Alberta by zone, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_labconf_by_zone{years}.csv' )

    elif row['figure'].startswith( 'Laboratory confirmed COVID-19 cases in Alberta by zone, 2019' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_labconf_by_zone.csv' )

    elif row['figure'].startswith( 'Laboratory confirmed COVID-19 cases in Alberta by zone, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_labconf_by_zone{years}.csv' )

    elif row['figure'] == 'Cumulative influenza vaccine doses administered by week in Alberta':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_cumu_vaccine_doses_by_week.csv' )

    elif row['figure'] == 'Influenza vaccine doses administered by week in Alberta':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_vaccine_doses_by_week.csv' )

    elif row['figure'] == 'Influenza vaccine coverage by week and age group in Alberta':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_vaccine_coverage_by_week_age.csv' )

    elif row['figure'] == 'Number of adverse events following immunization (AEFI) by condition reported in Alberta':

        output = extract_scatter( f"{target}/{row['file']}.json" )
        output.index.name = 'count'         # this is mislabeled!
        output.to_csv( f'{sys.argv[2]}/aefi_by_condition.csv' )

    elif row['figure'].startswith( 'Cumulative COVID-19 vaccine doses administered by week in Alberta, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_cumu_vaccine_doses_by_week{years}.csv' )

    elif row['figure'].startswith( 'COVID-19 vaccine doses administered by week in Alberta, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_vaccine_doses_by_week{years}.csv' )

    elif row['figure'].startswith( 'COVID-19 vaccine coverage by week and age group in Alberta, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_vaccine_doses_by_week_age{years}.csv' )

    elif row['figure'].startswith( 'Respiratory virus test positivity by week, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/respiratory_test_positivity_summary_by_week{years}.csv' )

    elif row['figure'].startswith( 'Respiratory virus test positivity by week for the ' ) and \
            row['figure'].endswith( 'compared to historical trends' ):

        match = season.search( row['figure'] )
        year = '.' + match.group(1) if match else ''

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/respiratory_test_positivity_by_week{years}.csv' )

    elif row['figure'].startswith( 'SARS-CoV-2 testing and positivity by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/sars-cov-2_testing_positivity_by_week.csv' )

    elif row['figure'].startswith( 'Influenza testing and positivity by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_testing_positivity_by_week.csv' )

    elif row['figure'].startswith( 'Respiratory syncytial virus testing and positivity by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/rsv_testing_positivity_by_week.csv' )

    elif row['figure'].startswith( 'Adenovirus testing and positivity by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/adenovirus_testing_positivity_by_week.csv' )

    elif row['figure'].startswith( 'Coronavirus testing and positivity by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/coronavirus_testing_positivity_by_week.csv' )

    elif row['figure'].startswith( 'Rhinovirus/enterovirus testing and positivity by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/rhinovirus_testing_positivity_by_week.csv' )

    elif row['figure'].startswith( 'Human metapneumovirus (hMPV) testing and positivity by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/hMPV_testing_positivity_by_week.csv' )

    elif row['figure'].startswith( 'Parainfluenza testing and positivity by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/parainfluenza_testing_positivity_by_week.csv' )

    elif row['figure'].startswith( 'Number of respiratory virus outbreaks by week, 2' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/respiratory_outbreaks_by_week.csv' )

    # TODO: 'Total hospital admissions, ICU admissions, and deaths among laboratory-confirmed seasonal influenza cases in Alberta by age group, 2023-2024'

    elif row['figure'].startswith( 'Number of weekly hospital admissions (ICU and non-ICU) and deaths due to laboratory-confirmed seasonal influenza, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_labconf_hospitalizations_deaths_by_week{years}.csv' )

    # TODO: 'Total hospital admissions, ICU admissions, and deaths among COVID-19 cases in Alberta by age group, 2023-2024'

    elif row['figure'].startswith( 'Number of weekly hospital admissions (ICU and non-ICU) and deaths due to COVID-19' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_hospitalizations_deaths_by_week.csv' )

    elif row['figure'].startswith( 'Total weekly laboratory-confirmed seasonal influenza cases by subtype in Alberta, 2019' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_labconf_cases_by_subtype.csv' )

    elif row['figure'].startswith( 'Total weekly laboratory-confirmed seasonal influenza cases by subtype in Alberta, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_labconf_cases_by_subtype{years}.csv' )

    elif row['figure'].startswith( 'Laboratory-confirmed seasonal influenza cases in Alberta since the 2009-2010 season' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/influenza_labconf_cases_by_week.csv' )

    elif row['figure'].startswith( 'Total weekly laboratory-confirmed RSV cases in Alberta, 2019' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/rsv_labconf_cases_by_week.csv' )

    elif row['figure'].startswith( 'Total weekly laboratory-confirmed RSV cases in Alberta, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/rsv_labconf_cases_by_week{years}.csv' )

    elif row['figure'].startswith( 'Number of weekly hospital admissions (ICU and non-ICU) and deaths due to laboratory-confirmed COVID-19, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_labconf_hospitalizations_deaths_by_week{years}.csv' )

    elif row['figure'].startswith( 'Total weekly laboratory-confirmed COVID-19 cases in Alberta, 2' ):

        comma = row['figure'].index(',')
        if comma <= 0:
            years = ''
        else:
            years = '.' + row['figure'][comma+1:].lstrip().rstrip()

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_labconf_cases_by_week{years}.csv' )

    elif row['figure'].startswith( 'Proportion of whole genome sequenced COVID-19 variants by type' ) or \
            (row['yaxis'] == 'COVID-19 Sequencing Results (Percentage)'):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/covid19_sequenced_variants_by_type.csv' )

    elif row['figure'].startswith( 'Population normalized quantity of SARS CoV-2 virus over time' ):

        output = extract_scatter( f"{target}/{row['file']}.json" )
        if row['xaxis']:
            output.index.name = row['xaxis']
        output.to_csv( f'{sys.argv[2]}/wastewater_percapita_quantity_by_week.csv' )

