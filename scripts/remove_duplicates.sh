#!/bin/sh

# grab a temp file
FILES=`mktemp`
HASHES=`mktemp`

# generate SHA256 sums of every file, then pull out the unique hash values
find -type f | xargs sha256sum | sort -t' ' -k2 > $FILES
cut -c-64 $FILES | sort | uniq -d > $HASHES

# since the file list is sorted by date, delete the second file on
while read ; do
	grep $REPLY $FILES | sed -e '1d;s/^.*  //' | xargs rm -v
done < $HASHES

rm $FILES $HASHES
