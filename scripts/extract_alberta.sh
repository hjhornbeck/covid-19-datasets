#!/bin/sh

TEMP=`mktemp`

# for each file on the command line
while (( $# )) ; do

	# we only process non-empty files
	if [ ! -s "$1" ] ; then 
		echo "$1: not a non-empty file"
		shift
		continue
	fi

	# check for XZ compressed archives
	INPUT="$1"
	if file "$1" | grep "XZ compressed data" >/dev/null 2>&1 ; then

		xzcat "$1" > $TEMP
		INPUT="$TEMP"
	fi

	# extract the timestamp
	TIME=`echo $1 | perl -ne 'print $1 if(/alberta\..*\.(\d+)\.html/)'`
	if [ -z "$TIME" ] ; then 
		echo "$1: could not parse the timestamp"
		shift
		continue
	fi
	HUMAN=`date --date="@$TIME" +"%Y/%Y%m%d_%H%M%S"`

	# make a directory with that name, if it doesn't already exist
	if ! [ -d $HUMAN ] ; then
		mkdir -p $HUMAN
	fi

	# cat the file into a perl script, saving all the JSON files
	perl -e '
my $switch;
my @rows;
my $write_header = -e "'$HUMAN'/map.tsv";
open( MAP, ">>", "'$HUMAN'/map.tsv" );		# set up the mapping
print MAP "file\tfigure\n" if( ! $write_header );
while (<>) {
	if( $switch == 1 ) {			# handle post-caption
		for my $file (@rows) { print MAP "$file\t$_" };
		@rows = ();
		undef $switch;
		}
	if( /^<p class="caption"/ ) { $switch = 1; next; }
	if( m|^<p><strong>(.*)</strong></p>$| ) { $switch = $1; next; }
	if( m|^<strong>(.*)</strong></p>$| ) { $switch = $1; next; }		# one exception!
	if( m|<script type="application/json" data-for="([^"]+)">(.+?)</script>| ) {
		push @rows, $1;
		open( FILE, ">", "'$HUMAN'/$1.json" );
		print FILE $2;
		close FILE;
		if( $switch ) {			# handle pre-caption
			for my $file (@rows) { print MAP "$file\t$switch\n" };
			@rows = ();
			undef $switch;
			}
		}
	}
for my $file (@rows) { print MAP "$file\t\n" };
close MAP;' "$INPUT"

	# change all the file timestamps
	touch --date="@"$TIME $HUMAN $HUMAN/*.json
	if [ -f "$HUMAN/*.json" ] ; then
		rm $HUMAN/*
	fi

	shift

done

# filtering out dupes? that's another script
