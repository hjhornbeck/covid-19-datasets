#!/bin/sh

# use a pipe to prevent issues when there are many files
ls -1 */*/map.tsv | while read FILE ; do

	# delete the map if all the associated JSON files were deleted
	DIR=`dirname $FILE`
	if [ `ls -1 $DIR | wc -l` -eq 1 ] ; then

		echo "$DIR contains only the mapping file, removing that file"
		rm $FILE

	fi
done

