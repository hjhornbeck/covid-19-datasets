#!/usr/bin/env python3

import os
import pandas as pd
import sys


# open up the two relevant series
#  (data/extracted/alberta/local_geographic_areas.cases.tsv)
#  (data/extracted/alberta/local_geographic_areas.vaccination.tsv)
# enumerate all possible times in the raw data
# subtract out the times already in the series

# for the remaining times:
#  use the map file to figure out which files contain geographic data
#  carefully parse those files for their data series
#   new series discovered? Create a new column and pad it out
# sort the updated dataframe
# save it out
