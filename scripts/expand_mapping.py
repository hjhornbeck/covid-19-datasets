#!/usr/bin/env python3

import json
import numpy as np
from os.path import basename,dirname
import pandas as pd
import sys

for filename in sys.argv[1:]:

    # read in the map file
    mapping = pd.read_csv(filename,sep='\t').set_index('file')

    # extract the base directory
    base = dirname(filename)

    # we need temp storage to juggle columns
    storage = { 'file':[], 'figure':[], 'geographic':[] }

    # for each file in the mapping
    for idx,row in mapping.iterrows():

        try:
            with open(f'{base}/{idx}.json','rt') as file:
                dataset = json.load( file )
        except Exception as e:
            print(f"Exception found with {base}, assuming it's a one-off and ignoring. {e}")
            continue

        if not dataset:             # skip invalid files
            continue
                                    # extract the axis labels from these Plotly charts, if possible
        if ('x' in dataset):
            if ('layout' in dataset['x']):      # standard line chart

                temp = dict([(key,val['title']) for key,val in dataset['x']['layout'].items() if type(val) == dict and 'title' in val])
                if ('legend' in temp) and ('text' in temp['legend']):            # tweak this one a bit
                    temp['legend'] = temp['legend']['text']

            elif ('calls' in dataset['x']):     # a map, we avoid those due to their complexity
                temp = { 'geographic':True }
        
        else:                       # catch the case where no labels could be extracted, so we can improve the script
            print( f'ERROR: could not extract the labels from {base}/{idx}.json, quitting.' )
            sys.exit(1)

        # add another row to storage, from what we've parsed
        for key in temp.keys():
            if key in storage:
                storage[key].append( temp[key] )
            else:
                storage[key] = [np.NaN]*len(storage['file']) + [temp[key]]

        storage['file'].append( idx )           # catch up this column
        storage['figure'].append( row['figure'] )

        # if we didn't parse a column, manually extend it
        for key in storage.keys():
            while len(storage[key]) < len(storage['file']):
                storage[key].append( np.NaN )

    # once finished, write it out
    pd.DataFrame( storage ).set_index('file').to_csv(filename,sep='\t')
